// include sdk headers to communicate with UE3
// WARNING: this header file can currently only be included once!
//   the SDK currently throws alot of warnings which can be ignored
#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <vector>
#include <ranges>
#include <algorithm>
#include <string>
#include <filesystem>
#include <SdkHeaders.h>

// for access to event manager
#include <Proxy/Events.h>
// for access to client/server
#include <Proxy/Network.h>
// for module api 
#include <Proxy/Modules.h>
// for logging in main file
#include <Proxy/Logger.h>

using namespace BLRevive;

const std::string MapExtension = ".fmap";
const std::string MapPathing = "_pathing";
const std::string MapSpawns = "_spawns";
const std::string MapAudio = "_audio";
// TODO: Add rest of map file types

const std::set<std::string> DefaultMaps =
{
	"deadlock",
	"piledriver1",
	"heavymetal",
	"vertigo",
	"containment",
	"vortex",
	"helodeck",
	"seaport",
	"decay",
	"evac",
	"trench",
	"metro",
	"centre",
	"shelter",
	"safehold",
	"lockdown",
	"deathmetal",
	"terminus",
	"outpost",
	"convoy",
	"rig",
	"crashsite",
	"gunrange_persistent"
};

std::string ToLower(std::string s) {
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

struct PlayListConfig {
	std::vector<std::string> PlayList;
};

static PlayListConfig playListConfig;
int loopCount = 0;

std::set<std::string> CustomMapFiles;

std::map<std::string, int> availableMaps = {
};

static std::string GetMapDirectoryPath() {
	static char* pathStr = nullptr;
	if (pathStr == nullptr) {
		std::string path = Utils::FS::BlrBasePath() + "FoxGame\\CookedPCConsole\\Maps\\";
		pathStr = new char[path.length() + 1];
		strcpy(pathStr, path.c_str());
		return path;
	}
	else {
		return std::string(pathStr);
	}
}

static std::set<std::string> GetCustomMapFiles() {
	LDebug("Getting all custom map files");
	if (!CustomMapFiles.empty()) {
		LDebug("We've already fetched them previously, returning cached value");
		return CustomMapFiles;
	}

	LDebug("Get map directory");
	auto mapDirectory = GetMapDirectoryPath();

	LDebug("Loop through files");
	for (const auto& entry : std::filesystem::recursive_directory_iterator(mapDirectory))
	{
		auto filename = ToLower(entry.path().filename().string());
		if (filename.ends_with(MapExtension)) {
			CustomMapFiles.insert(filename);
		}
	}
	LDebug("Return list of map files");
	return CustomMapFiles;
}

static std::string GetConfigPath() {
	static char* pathStr = nullptr;
	if (pathStr == nullptr) {
		std::string path = Utils::FS::BlreviveConfigPath();
		pathStr = new char[path.length() + 1];
		strcpy(pathStr, path.c_str());
		return path;
	}
	else {
		return std::string(pathStr);
	}
}

static std::string GetOutputPath() {
	std::string outputPath = GetConfigPath() + "playlist_manager/";

	struct stat info;
	if (stat(outputPath.c_str(), &info) == 0) {
		// path exists
		if (info.st_mode & S_IFDIR) {
			// path exists and is a dir
			return outputPath;
		}
		else {
			LError(std::format("{0} exists but it is not a directory", outputPath));
			return "";
		}
	}
	else {
		// path do not exist
		if (CreateDirectory(outputPath.c_str(), nullptr)) {
			return outputPath;
		}
		else {
			LError(std::format("cannot create directory {0}", outputPath));
			return "";
		}
	}
}

static json GetJsonValue(json& input, json& defaultOverlay, std::string param) {
	json val;
	try {
		val = input[param];
		if (val.is_null()) {
			LWarn(std::format("{0} not found in config, using default value", param));
			val = defaultOverlay[param];
		}
	}
	catch (json::exception e) {
		throw(e);
	}
	return val;
}

static json DefaultConfigJson() {
	json defaultConfig;
	defaultConfig["PlayList"] = json::array();
	return defaultConfig;
}

static PlayListConfig ServerConfigFromJson(json input) {
	PlayListConfig config;
	json defaultConfig = DefaultConfigJson();
	try {
		json playListArray = GetJsonValue(input, defaultConfig, "PlayList");
		for (int i = 0; i < playListArray.size(); i++)
		{
			json mapName = playListArray[i];
			if (mapName.is_null()) {
				LWarn(std::format("the #{0} playlist map name was not provided", i));
			}
			else
			{
				config.PlayList.push_back(ToLower(mapName));
			}
		}
	}
	catch (json::exception e) {
		throw(e);
	}
	return config;
}

static void WriteConfig(json& toWrite, std::string path) {
	std::ofstream output(path);
	if (!output.is_open()) {
		LError(std::format("failed writing config to {0}", path));
		return;
	}
	output << toWrite.dump(4) << std::endl;
	output.close();
	return;
}

static PlayListConfig PlayListFromFile() {
	std::string outputPath = GetOutputPath();
	if (outputPath.length() == 0) {
		LError(std::format("cannot load config from {0}", outputPath));
		return ServerConfigFromJson(DefaultConfigJson());
	}

	std::string serverConfigPath = std::format("{0}{1}", outputPath, "playlist_config.json");
	std::ifstream input(serverConfigPath);
	if (!input.is_open()) {
		LDebug(std::format("{0} does not exist, writing default config", serverConfigPath));
		json config = DefaultConfigJson();
		WriteConfig(config, serverConfigPath);
		return ServerConfigFromJson(config);
	}

	try {
		json inputJson = json::parse(input);
		input.close();
		return ServerConfigFromJson(inputJson);
	}
	catch (json::exception e) {
		LError(std::format("failed parsing {0}, using default config", serverConfigPath));
		LError(e.what());
		input.close();
		return ServerConfigFromJson(DefaultConfigJson());
	}
}

UFoxDataProvider_MapInfo* CreateMapInfo(std::string mapName)
{
	LDebug("Creating new MapInfo for {}", mapName);
	auto customMapFiles = GetCustomMapFiles();

	if (customMapFiles.size() == 0)
	{
		LDebug("No custom map files found");
		return NULL;
	}

	auto mapNameFStr = FString(mapName.c_str());
	auto newMapInfo = UObject::CreateInstance<UFoxDataProvider_MapInfo>();

	//Base
	if (customMapFiles.contains(mapName + MapExtension))
	{
		LDebug("Base file found");
		auto base = FName(mapName.c_str());
		newMapInfo->Name = base;
		newMapInfo->BaseLevelStreamNames.push_back(base);
		newMapInfo->MapName = mapNameFStr;
		newMapInfo->FriendlyName = mapNameFStr;
	}
	else
	{
		return NULL;
	}

	//Pathing
	if (customMapFiles.contains(mapName + MapPathing + MapExtension))
	{
		LDebug("Pathing file found");
		auto pathing = FName((mapName + MapPathing).c_str());
		newMapInfo->BaseLevelStreamNames.push_back(pathing);
	}

	//DM, TDM
	if (customMapFiles.contains(mapName + MapSpawns + MapExtension))
	{
		LDebug("Spawns file found");
		auto spawns = FName((mapName + MapSpawns).c_str());
		newMapInfo->DMStreamNames.push_back(spawns);
		newMapInfo->TDMStreamNames.push_back(spawns);
	}

	//Audio
	if (customMapFiles.contains(mapName + MapAudio + MapExtension))
	{
		LDebug("Audio file found");
		auto audio = FName((mapName + MapAudio).c_str());
		newMapInfo->BaseLevelStreamNames.push_back(audio);
	}

	//TODO: Rest of the files

	LDebug("Done creating MapInfo");
	return newMapInfo;
}

/// <summary>
/// Thread thats specific to the module (function must exist and export demangled!)
/// </summary>
extern "C" __declspec(dllexport) void ModuleThread()
{
	// put your code here
	if (!Utils::IsServer()) {
		return;
	}

	playListConfig = PlayListFromFile();

	auto mgr = Event::Manager::GetInstance();

	mgr->RegisterHandler({ Event::ID(52580, -1), [](Event::Info i) { //UpdateGameSettings
		LDebug("UpdateGameSettings");
		LDebug("Event: {}, {}", i.Object->GetFullName(), i.Function->Name);
		LDebug("Event: Function {}, Object {}", i.Function->ObjectInternalInteger, i.Object->ObjectInternalInteger);
		LDebug(" --------------------- Starting loop {} --------------------- ", loopCount);

		AFoxGame* game = (AFoxGame*)i.Object;

		auto datastoreGame = game->PlaylistsDataStore;
		auto menuItems = game->MenuItemsDataStore;

		LDebug("NextMapIndex {}", game->FGRI->NextMapIndex);
		LDebug("NextNextMapIndex {}", game->FGRI->NextNextMapIndex);
		LDebug("PlaylistCycleIndex {}", datastoreGame->PlaylistCycleIndex);

		availableMaps.clear();
		for (int i = 0; i < menuItems->MapProviders.Count; i++)
		{
			auto mapName = ToLower(((UFoxDataProvider_MapInfo*)menuItems->MapProviders[i])->MapName.ToChar());
			LDebug("Inserting {} : {} | Total {}", i, mapName, availableMaps.size());
			availableMaps.insert(std::pair<std::string, int>(mapName, i));
		}

		playListConfig = PlayListFromFile();

		std::set<std::string> newMaps;
		for (auto map : playListConfig.PlayList)
		{
			if (!availableMaps.contains(map) && !newMaps.contains(map))
			{
				newMaps.insert(map);
				auto newMapInfo = CreateMapInfo(map);
				if (newMapInfo)
				{
					LDebug("ADDING NEW MAPPROVIDER: {}", map);
					menuItems->MapProviders.push_back(newMapInfo);
					LDebug("ADDED MAPPROVIDER");
				}
			}
		}

		if (loopCount == 0) {

			//Extra stuff
			game->MaxBotCount = game->GetIntOption(game->ServerOptions, "NumBots", game->MaxBotCount);
			game->MaxPlayers = game->GetIntOption(game->ServerOptions, "MaxPlayers", game->MaxPlayers);

			LDebug("ServerOptions = {}", game->ServerOptions);
			LDebug("MaxPlayers = {}", game->MaxPlayers);
			LDebug("MaxBotCount = {}", game->MaxBotCount);
		}


		if (loopCount > 1 && !playListConfig.PlayList.empty())
		{
			auto playListSize = playListConfig.PlayList.size();
			if (datastoreGame->PlaylistCycleIndex > playListSize - 1)
			{
				datastoreGame->PlaylistCycleIndex = 0;
			}

			auto nextMapName = playListConfig.PlayList[datastoreGame->PlaylistCycleIndex];
			auto nextNextMapName = playListConfig.PlayList[(datastoreGame->PlaylistCycleIndex + 1) % playListSize];

			LDebug("nextMapName {} | nextNextMapName {}", nextMapName, nextNextMapName);

			auto nextMapIndex = availableMaps[nextMapName];
			auto nextNextMapIndex = availableMaps[nextNextMapName];

			game->FGRI->NextMapIndex = nextMapIndex; //THIS WORKS TO CHANGE NEXT MAP!
			game->FGRI->NextNextMapIndex = nextNextMapIndex;
		}

		if (loopCount < 5) {

			LDebug(" ----------------- MENUITEMS MAP PROVIDERS, COUNT {} ----------------- ", menuItems->MapProviders.Count);
			for (auto& mapProvider : menuItems->MapProviders)
			{
				auto mapInfo = (UFoxDataProvider_MapInfo*)mapProvider;
				LDebug("Name = {} | {}", mapInfo->MapName, mapInfo->FriendlyName);
			}
		}
		LDebug(" --------------------- Finished loop {} --------------------- ", loopCount);

		loopCount++;

		// return false to keep action active
		return false;
	}, Event::Action::PRE });
}

/// <summary>
/// Module initializer (function must exist and export demangled!)
/// </summary>
/// <param name="data"></param>
extern "C" __declspec(dllexport) void InitializeModule(Module::InitData * data)
{
	// check param validity
	if (!data || !data->EventManager || !data->Logger) {
		LError("module initializer param was null!"); LFlush;
		return;
	}

	// initialize logger (to enable logging to the same file)
	Logger::Link(data->Logger);

	// initialize event manager
	// an instance of the manager can be retrieved with Events::Manager::Instance() afterwards
	Event::Manager::Link(data->EventManager);

	LDebug("Playlist Manager First log!");
}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

